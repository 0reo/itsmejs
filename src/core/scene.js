/**
Scene class.  Scenes can be thought of as levels, locations, etc
*/
ITSME.IMScene = class IMScene extends THREE.Scene {
  /**
  * Make the scene
  * @arg {string} name Name of the scene
  * @arg {THREE.Scene} [scene] Exsisting THREE.js scene
  */
  constructor(name, scene) {

    if (!scene){
      super();
    } else {
      THREE.Scene.apply(this);
    }

    //models have a name and a reference to their geometry
    this.name = name | "";
    this.models = [];
    this.lights = new THREE.Object3D;
    this.add(this.lights)
    this.skyBox = null;

  }

/**
* Add a model to the scene
* @arg {IMModel} model IMModel to add to scene
*/
  addModel(model){
    this.add(model)
    this.models.push({
      name:model.name,
      geometry:model
    });
  }

  /**
  * Create and add a model to the scene
  * @arg {string} modelObj name of the mesh
  * @arg {string} texture name of texture file
  * @arg {string} animation Name of default animation to apply to model
  * @arg {string} material Material to apply to model
  * @arg {string} name Name of model
  */
  addNewModel(modelObj, texture, animation, material, name){
    let model = new ITSME.IMModel(objPath, texturePath, animation, material, name);
    this.addModel(model);
  }

  /*addScene(objPath, texturePath, animation, material, name){
    let model = new ITSME.IMModel(objPath, texturePath, animation, material, name);
    model.scene = new ITSME.IMScene(model.scene);
    model.scene.models = this.models;
    model.scene.lights = this.lights;
    this = model.scene;
    this.updateMatrix();
    //should this be this.addModel?
    this.models.push({
      name:model.name,
      geometry:model
    });
  }*/

/*
  /**
  * Create and add an IMAvatar to the scene
  * @arg {string} modelObj name of the mesh
  * @arg {string} texture name of texture file
  * @arg {string} animation Name of default animation to apply to model
  * @arg {string} material Material to apply to model
  * @arg {string} name Name of model
  *
  addNewAvatar(url, animation, material){
    let avatar = new ITSME.IMModel(objPath, texturePath, animation, material);
    this.addModel(avatar);
  }
*/


/**
* Remove model from scene
* @arg {string} name Name of model to remove
*/
  removeModel(name){
    let selectedObject = this.getObjectByName(name);
    this.remove( selectedObject );
  }

 /**
 Should the scene cast shadows?
 * @arg {Boolean} show Show shadow?
 */
  showShadows(show) {
	  this.traverse( function ( child ) {
  		if (child instanceof THREE.Mesh) {
  			child.castShadow = show;
  			child.receiveShadow = show;
  			// child.material.needsUpdate = true;
  	  }
	  });
  }

  /**
  * create a new light and add it to the scene
  * @arg {string} type Type of light
  * @arg {string} [name] Name of light.  Good luck accessing the light if you
  * don't name it
  * @arg {Number} [color] Hex color code.  Optional depending on Type.  Look up
  * Light documentation in THREE.js
  * @arg {string} [intensity] Lights intensity.  Optional depending on Type.  Look up
  * Light documentation in THREE.js
  */
  makeLight(type, name, color, intensity){
    switch (type){
      case "ambient":
        this.lights.add(new ITSME.LIGHTING[type](color));
      break;
      case "directional":
        this.lights.add(new ITSME.LIGHTING[type](color, intensity));
      break;
      default:
        return;
      break;
    }
    if (name !== undefined){
      let newest = this.lights.children.length - 1;
      this.lights.children[newest].name = name;
    }

  }

  /** add an exsisting light to a scene
  * @type {ITSME.LIGHTING}
  */
  set light(light){
    this.lights.add(light)
  }

  /** access light based on name
  * @arg {string} name Name of light object
  */
  getLight(name){
    return this.lights.getObjectByName(name);
  }

  /** set up initial control movement.  Currently dependant on OrbitControls
  * @arg {Number} minDist Minimum zoom distance
  * @arg {Number} maxDist Maximum zoom distance
  * @arg {Number} maxAng Maximum rotation angel in Radians
  */
  setMotionRange(minDist, maxDist, maxAng){
    ITSME.viewer.camera.updateProjectionMatrix();

    ITSME.viewer.oControls.minDistance = minDist;
    ITSME.viewer.oControls.maxDistance = maxDist;
    ITSME.viewer.oControls.maxPolarAngle = maxAng;
  }

/**
* Set camera position
* @arg {Number} x X position
* @arg {Number} y Y position
* @arg {Number} z Z position
*/
  positionCamera(x, y, z){
    ITSME.viewer.oControls.object.position.set(z, y, z);
  }

  /**
  * Look at something
  * @arg {THREE.Object3D} target object.
  * @arg {Number} xOffset offset from target
  * @arg {Number} yOffset offset from target
  * @arg {Number} zOffset offset from target
  */
  setCameraFocus(target, xOffset, yOffset, zOffset){
    ITSME.viewer.oControls.target.set(target.position.x+xOffset, target.position.y+yOffset, target.position.z+zOffset);
    ITSME.viewer.oControls.object.position.copy(target.position);//(-1225, -1385, 100);

  }

/**
* Where to position audio listener
* @arg {Number} x X position
* @arg {Number} y Y position
* @arg {Number} z Z position
*/
  positionListener(x, y, z){
    ITSME.viewer.camera.listener.position.set(x, y, z);
  }

/**
* The type of shadow rendering the scene will utilize
* @arg {string} type
* @type {ITSME.SHADOWS}
*/
  set shadowType(type){
    switch (type) {
      case "soft":
        ITSME.viewer.renderer.shadowMap.type = ITSME.SHADOWS[type]
      break;
      default:
        return false;
      break;
    }

  }

/**
* Make a skybox
* @arg {string} path Path to images, relative to testure asset path
* @arg {Number} skySize Size of skybox
* @arg {THREE.Vector3} position Position of skybox in world coordinates
* @arg {string} name Name of skybox
*/
  makeSkybox(path, skySize, position, name){
    let texloader = new THREE.TextureLoader();
    texloader.crossOrigin = 'anonymous'

    let imagePrefix = `${root_url}/assets/textures/${path}/`;
  	//let directions  = ["lf", "rt", "up", "dn", "ft", "bk"];
  	let directions  = ["posx", "negx", "posy", "negy", "posz", "negz"];
  	let imageSuffix = ".jpg";
  	let skyGeometry = new THREE.BoxGeometry( skySize, skySize, skySize );

  	let materialArray = [];
  	let loaded = false;
  	for (let i = 0; i < 6; i++){
  	  let tex = null;

      materialArray.push(new THREE.MeshBasicMaterial({
        map: texloader.load( imagePrefix + directions[i] + imageSuffix, function(){
            if (!loaded && materialArray.length == 6){
              console.log(loaded);
              loaded = true;

              let skyMaterial = new THREE.MultiMaterial(materialArray);
              ITSME.viewer.activeScene.skyBox = new THREE.Mesh(skyGeometry, skyMaterial);
              ITSME.viewer.activeScene.skyBox.name = name;
              ITSME.viewer.activeScene.skyBox.position.copy(position);
              ITSME.viewer.activeScene.add(ITSME.viewer.activeScene.skyBox);

            }
        }),
        side: THREE.BackSide}
      ));
    }
  }





  render() {}
}
